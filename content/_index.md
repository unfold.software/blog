---
title: Home
menu:
  - main
  - sidebar
weight: -270
---

```scala
def unfold[A, S](init: S)(f: (S) => Option[(A, S)]): CC[A]
```
