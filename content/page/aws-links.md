---
title: AWS links
description: Some AWS links to explore
date: 2020-04-30T23:08:32+02:00
tags: ["aws"]
---

AWS Lambda Getting Started
https://aws.amazon.com/lambda/getting-started/

Getting Started with Amazon DynamoDB
https://aws.amazon.com/dynamodb/getting-started/

Getting Started with AWS X-Ray
https://aws.amazon.com/xray/getting-started/

Amazon API Gateway Getting Started
https://aws.amazon.com/api-gateway/getting-started/

Best Practices for Working with AWS Lambda Functions
https://docs.aws.amazon.com/lambda/latest/dg/best-practices.html

Serverless Architectures with AWS Lambda: Overview and Best Practices
https://aws.amazon.com/blogs/architecture/serverless-architectures-with-aws-lambda-overview-and-best-practices

AWS Security Best Practices: Config Rules for AWS Lambda Security
https://www.puresec.io/blog/aws-security-best-practices-config-rules-lambda-security

Serverless Microservice Patterns for AWS
https://www.jeremydaly.com/serverless-microservice-patterns-for-aws/

AWS re:Invent 2018: Amazon DynamoDB Deep Dive: Advanced Design Patterns for DynamoDB
https://www.youtube.com/watch?v=HaEPXoXVf2k

Building API-Driven Microservices with Amazon API Gateway
https://www.youtube.com/watch?v=xkDcBssNd1g

Serverless by Design
https://sbd.danilop.net
