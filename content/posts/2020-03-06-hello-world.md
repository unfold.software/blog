---
title: "Hello World"
date: 2020-03-06T15:12:37+01:00
authors: ["dawid.weckowski"]
---

This is a first post for a start :rocket:

```scala
object HelloWorld extends App {
    println("Hello World!")
}
```
