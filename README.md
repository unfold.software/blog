## Unfold Software Blog

### Ideas to cover in blog

1. [ ] Apache Spark Catalyst logical optimizations

    * https://medium.com/teads-engineering/spark-performance-tuning-from-the-trenches-7cbde521cf60
    * https://www.slideshare.net/cfregly/advanced-apache-spark-meetup-project-tungsten-nov-12-2015
    * https://mallikarjuna_g.gitbooks.io/spark/spark-sql-catalyst-Optimizer.html

    1. [ ] Speeding-up UDFs with custom native functions
         * https://medium.com/@fqaiser94/udfs-vs-map-vs-custom-spark-native-functions-91ab2c154b44

    2. [ ] Spark star schema enhancements

2. [ ] Apache Spark Catalyst physical optimizations

    1. [ ] Cost-Based Optimizer

3. [ ] New Scala tools: Fury, Bloop, Metals...

    * https://www.youtube.com/watch?v=PHjAKgdUMWI
    * https://scalameta.org/metals/
    * https://scalacenter.github.io/bloop/

4. [ ] Building Enterprise Data Lake

    * Delta Lake
    * StreamSets
    * Hive 3.x
    * AWS

    1. [ ] Facilitating Data-Driven Organization with Data Lake

5. [ ] 2FA with HOTP in Play or Akka HTTP

6. [ ] Schema evolution in Big Data: Avro, Parquet, Hive

7. [ ] Unified view on structured and unstructured data: Hive + Elasticsearch

8. [x] Scala context bound as "has-a" relationship

    * https://docs.scala-lang.org/tutorials/FAQ/context-bounds.html
    * https://stackoverflow.com/questions/2982276/what-is-a-context-bound-in-scala

9. [ ] Type-safe builder pattern

    * https://pedrorijo.com/blog/typesafe-builder/
    * can it be done by annotation?
    * can we improve it using Scala 3?

10. [x] Scala generics shading error

11. [ ] Case objects / enums from configs

12. [x] Beware of casting Spark Dataset with "as" method

13. [ ] Case-sensitive spark table creation and ingestion

14. [ ] Spark + Hive locally: permission exception

    * https://stackoverflow.com/questions/50232040/error-running-spark-in-a-scala-repl-access-denied-org-apache-derby-security-sy
